﻿using Microsoft.AspNetCore.Mvc;

namespace Barrale_mvc.Controllers
{
    public class VideosController : Controller
    {
        public IActionResult Index()
        {
            return View("videos");
        }
    }
}
